//
//  AddItemViewController.swift
//  ToDo
//
//  Created by PAOLA GUAMANI on 9/5/18.
//  Copyright © 2018 Pao Stephanye. All rights reserved.
//

import UIKit

class AddItemViewController: UIViewController {

    @IBOutlet weak var titleTextField: UITextField!
    
    @IBOutlet weak var locationTextField: UITextField!
    
    @IBOutlet weak var descriptionTextField: UITextField!
    
    var itemManager: ItemManager? //signifca que espera un itemManager
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func cancelButtonPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        //poner vista navigationController.push, salir .pop
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        let itemTitle = titleTextField.text ?? ""
        let itemLocation = locationTextField.text ?? ""
        let itemDescription = descriptionTextField.text ?? ""
        
        let item = Item(
            title: itemTitle,
            location: itemLocation,
            description: itemDescription
        )
        
        /*let alert = UIAlertController(title: "Validacion", message: "Titulo y descripcion son obligatorios", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
        //alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        
        if itemTitle == "" || itemDescription == ""{
            self.present(alert, animated: true)
        }else{
            let item = Item(
                title: itemTitle,
                location: itemLocation,
                description: itemDescription
            )
            let alert2 = UIAlertController(title: "Add", message: "Item agregado", preferredStyle: .alert)
            
            alert2.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
            self.present(alert2, animated: true)
            titleTextField.text = ""
            locationTextField.text = ""
            descriptionTextField.text = ""
        }*/
        if(itemTitle == ""){
            showAlert(title: "Error", message: "Title is required")
        }else if(itemDescription == ""){
            showAlert(title: "Error", message: "Description is required")
        }else{
            itemManager?.toDoItems += [item]
        }
        //mostrar pantalla mostrar push, salir pop
        navigationController?.popViewController(animated: true)
    }
    
    func showAlert(title:String, message:String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        //handle es bloque de codigo q se ejecuta cuando demos clic en boton, alert
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
}
