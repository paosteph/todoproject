//
//  ItemManager.swift
//  ToDo
//
//  Created by PAOLA GUAMANI on 9/5/18.
//  Copyright © 2018 Pao Stephanye. All rights reserved.
//

import Foundation

class ItemManager {
    
    var toDoItems:[Item] = []
    var donItems:[Item] = []
    
    //pasar cosas de to a Done
    func checkItem(index: Int){
        let item = toDoItems.remove(at: index)
        donItems += [item]
    }
    
    func unCheckItem(index: Int){
        let item = donItems.remove(at: index)
        toDoItems += [item]
    }
    
}
