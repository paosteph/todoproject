//
//  Item.swift
//  ToDo
//
//  Created by PAOLA GUAMANI on 9/5/18.
//  Copyright © 2018 Pao Stephanye. All rights reserved.
//

import Foundation

struct Item{
    
    let title: String
    let location : String
    let description : String
    
}
