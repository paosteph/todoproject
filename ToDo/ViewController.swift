//
//  ViewController.swift
//  ToDo
//
//  Created by PAOLA GUAMANI on 8/5/18.
//  Copyright © 2018 Pao Stephanye. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    let itemManager = ItemManager()
    
    @IBOutlet weak var itemsTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        /*let item1 = Item(title: "To Do 1", location: "Office", description: "Do something")
        let item2 = Item(title: "To Do 2", location: "House", description: "Feed the cat")
        let item3 = Item(title: "To Do 3", location: "Brasil", description: "Go to the beach")
        let item4 = Item(title: "To Do 4", location: "Narnia", description: "Eat")
        
        itemManager.toDoItems = [item1, item2, item3]
        
        itemManager.donItems = [item4]*/
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        itemsTableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toAddItemSegue"{
            let destination = segue.destination as! AddItemViewController //segue puede tener donde esta y donde va
            destination.itemManager = itemManager
        }
        if segue.identifier == "toItemInfoSegue"{
            let destination = segue.destination as! IteInfoViewController
            let selectedRow = itemsTableView.indexPathsForSelectedRows![0]
            destination.itemInfo = (itemManager, selectedRow.row)
        }
        
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return itemManager.toDoItems.count
        }
        return itemManager.donItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //reusablecell depende del telefono se pueden cargar cierto num de celdas, y esas
        //carga en memoria, y si hago scroll carga la siguiente.
        let cell = tableView.dequeueReusableCell(withIdentifier: "itemCell") as! ItemTableViewCell
        
        if indexPath.section == 0 {
            cell.titleLabel.text = itemManager.toDoItems[indexPath.row].title //indexpath es indice de fila
            cell.locationLabel.text = itemManager.toDoItems[indexPath.row].location
        } else{
            cell.titleLabel.text = itemManager.donItems[indexPath.row].title
            cell.locationLabel.text = ""
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //funcion para que al select row encuentre el indice
        if indexPath.section == 0{
            performSegue(withIdentifier: "toItemInfoSegue", sender: self)
        }
        
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String?{
        return indexPath.section == 0 ? "Check" : nil
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if indexPath.section == 0{
            itemManager.checkItem(index: indexPath.row)
        }else{
            itemManager.unCheckItem(index: indexPath.row)
        }
        itemsTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
            return section == 0 ? "To Do" : "Done"
    }
    
    //prototype cell es como un view, se puede poner celdas botones, y mas
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //uitableviewdelegate : parte grafica e la tabla e intencion con usuario
}

